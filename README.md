# `labotekh.labotekh.ws_init`
The `labotekh.labotekh.ws_init` role initializes the variables used by the `ws_*` Labotekh roles.

## How to create services configurations
This role uses two variables to determine where to find the services to deploy. `services_variables_path` sets the path to the sites configuration folders. `services_variables_file` sets the name of the file containing the variables for each service (ex: `vars.yml`). Each service configuration must have the following directory structure :
```
<services_variables_path>
├── <service_name>
│   ├── backup_restore.sh               # (optional)
│   ├── backup_save.sh                  # (optional)
│   ├── docker-compose.yml
│   └── <services_variables_file>
└── <service_name_2>
    └ ...
```

`backup_save.sh` will be executed to save the service database in the `files` folder which will be archived, cyphered and saved to S3. Same goes for `backup_restore.sh` but at the opposite to reload database data from data in `files` folder. Example of save and restore script with a Django project :
```sh
# backup_save.sh
#!/bin/sh
touch files/backup.json
docker-compose exec -u runner web python3 manage.py dumpdata > files/backup.json

# backup_restore.sh
#!/bin/sh
docker-compose exec -u runner web python3 manage.py migrate admin
docker-compose exec -u runner web python3 manage.py loaddata files/backup.json
```

The `docker-compose.yml` is used to launch the service. You can find below an example of `docker-compose.yml` for a simple Django project. Note that all variables defined in the `services_variables_file` are usable with Jinja2 syntax as well as the variables required for all roles of the collection.

```yml
version: "3.9"

services:
  db:
    image: postgres:latest
    restart: always
    volumes:
      - {{ root_directory }}/{{ item.value.service_name }}/data:/var/lib/postgresql/data
    env_file:
      - {{ item.value.service_name }}.env
    networks:
      - db_link

  web:
    image: {{ item.value.registry_image }}:latest
    restart: always
    entrypoint: /code/entrypoint_master.sh
    command: gunicorn liam.wsgi:application -b :8000
    volumes:
      - {{ root_directory }}/{{ item.value.service_name }}/files:/code/files:rw
    env_file:
      - {{ item.value.service_name }}.env
    depends_on:
      - db
    networks:
      - nginx_link
      - db_link

networks:
  db_link:
    driver: bridge
  nginx_link:
    driver: bridge
```

The `services_variables_file` is a YAML file containing needed variables for each service. Use the following template as a reference to fill in all the values.

```yml
<service_name>:                         # Must be equal as service_name field
  root_domain:    xxxx.xx               # ex: test.com
  subdomain:      <str>                 # ex: "thing", service FQDN will be thing.test.com. Is empty if root_domain is different from host root_domain
  service_name:   <slug>                # A slug to name the service
  registry_image: <url>                 # ex: registry.gitlab.com/group/project
  cors:           <True | False>        # Access-Control-Allow-Origin "*" and Access-Control-Allow-Methods 'GET, POST, OPTIONS'
  db_password:    <str>                 # Can be an empty string to prevent creation of db related vars in .env file
  custom_env:                           # These variables will be added to the .env file which make them available in your service
  backup:                               # These variables define the backup policy for the service
    dayly: 3
    weekly: 3
    monthly: 1
    yearly: 0
  environment
    - name:       <env_var_name_1>
      value:      <env_var_value_1>
    - name:       <env_var_name_2>
      value:      <env_var_value_2>
```

## Additionnal informations
Your Docker image should listen on port 8000 in order to receive Web requests from the reverse proxy.

The TLS certificate generation is handled by the collection and the DNS registration is done through OVH. Look at the documentation of the collection to setup your variables.

Don't forget to use the `files` directory to store your static and media files which will be served by the reverse proxy. Your backup and restore scripts will also use this directory to save and restore your database. Also prefix the files generated and used by your scripts with `backup` so they can be deleted by cleaning tasks.
